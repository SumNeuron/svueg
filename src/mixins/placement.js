export default {
  props: {
    x: {
      type: Number,
      default: 0
    },

    y: {
      type: Number,
      default: 0
    },

    width: {
      type: Number,
      default: 0
    },

    height: {
      type: Number,
      default: 0
    },

    translateX: {
      type: Number,
      default: 0
    },

    translateY: {
      type: Number,
      default: 0
    },

    rotateX: {
      type: Number,
      default: 0
    },

    rotateY: {
      type: Number,
      default: 0
    },

    rotateD: {
      type: Number,
      default: 0
    },

    scale: {
      type: Number,
      default: 1
    }
  },
  computed: {
    translate() {
      return `translate(${this.translateX}, ${this.translateY})`
    },
    rotate() {
      return `rotate(${this.rotateD}, ${this.rotateX}, ${this.rotateY})`
    },
    _scale() {
      return `scale(${this.scale})`
    },
    transform() {
      return `${this.translate} ${this.rotate} ${this._scale}`
    }
  }
}
