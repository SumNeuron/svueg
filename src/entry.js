import Vue from 'vue';

// mixins
import placement from './mixins/placement.js';

// directives
import resize from './directives/resize.js';

// components
import svuegG from './components/g.vue';
import svuegRect from './components/rect.vue';
import svuegText from './components/text.vue';


const components = {
  svuegG,
  svuegRect,
  svuegText
}


function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

  Vue.directive('resize', resize);

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
  Vue.directive('resize', resize);
  
}


export default components
export { placement }
export { resize }
export { svuegG, svuegRect, svuegText }
